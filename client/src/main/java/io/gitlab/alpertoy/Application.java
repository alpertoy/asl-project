package io.gitlab.alpertoy;

import picocli.CommandLine;

public class Application {

	public static void main(String[] args) {
		GeneratorCommand command = new GeneratorCommand();
		CommandLine cmd = new CommandLine(command);
		cmd.setUnmatchedOptionsArePositionalParams(true);
		cmd.execute(args);

	}

}
