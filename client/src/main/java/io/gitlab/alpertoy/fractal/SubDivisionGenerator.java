package io.gitlab.alpertoy.fractal;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.io.IOUtils;

public class SubDivisionGenerator implements Runnable {

	private final URL server;
	private final BufferedImage image;

	public SubDivisionGenerator(URL server, BufferedImage image) {
		this.server = server;
		this.image = image;
	}

	@Override
	public void run() {
		HttpURLConnection connection = null;

		try {
			connection = (HttpURLConnection) server.openConnection();
			connection.setRequestMethod("GET");
			InputStream in = connection.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(in, baos);

			in.close();
			baos.close();

			BufferedImage requestImage = Imaging.getBufferedImage(baos.toByteArray());

			Graphics graphics = image.getGraphics();
			graphics.drawImage(requestImage, 0, 0, requestImage.getWidth(), requestImage.getHeight(), null);
			System.out.println("One image request completed");
		} catch (IOException | ImageReadException exception) {
			System.err.println("ImageReadException: "
					+ exception.getMessage());
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
