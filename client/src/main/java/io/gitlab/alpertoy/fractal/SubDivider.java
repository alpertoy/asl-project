package io.gitlab.alpertoy.fractal;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class SubDivider {

	public class SubDivision {
		private final int originalX;
		private final int originalY;
		private final BufferedImage image;

		public SubDivision(int originalX, int originalY, BufferedImage image) {
			this.originalX = originalX;
			this.originalY = originalY;
			this.image = image;
		}

		public int getOriginalMinX() {
			return originalX;
		}

		public int getOriginalMinY() {
			return originalY;
		}

		public BufferedImage getImage() {
			return image;
		}
	}

	private final int horizontalDivisions;
	private final int verticalDivisions;

	public SubDivider(int horizontalDivisions, int verticalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
		this.verticalDivisions = verticalDivisions;
	}

	public List<SubDivision> divide(BufferedImage image) {
		List<SubDivision> subImages = new ArrayList<>();
		int subDivisionWidth = image.getWidth() / horizontalDivisions;
		int subDivisionHeight = image.getHeight() / verticalDivisions;

		for (int i = 0; i < horizontalDivisions; i++) {
			for (int j = 0; j < verticalDivisions; j++) {
				int width = subDivisionWidth;
				int height = subDivisionHeight;
				int x = subDivisionWidth * i;
				int y = subDivisionHeight * j;

				if (i == 0 && j == 0) {
					width += image.getWidth() % subDivisionWidth;
					height += image.getHeight() % subDivisionHeight;
				}

				BufferedImage subImage = image.getSubimage(x, y, width, height);
				subImages.add(new SubDivision(x, y, subImage));
			}
		}

		return subImages;
	}

}
