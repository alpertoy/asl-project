package io.gitlab.alpertoy;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.imaging.ImageFormats;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;

import io.gitlab.alpertoy.fractal.SubDivider;
import io.gitlab.alpertoy.fractal.SubDivisionGenerator;
import picocli.CommandLine.Parameters;

public class GeneratorCommand implements Runnable {

	@Parameters(index = "0")
	private Double min_c_re;

	@Parameters(index = "1")
	private Double min_c_im;

	@Parameters(index = "2")
	private Double max_c_re;

	@Parameters(index = "3")
	private Double max_c_im;

	@Parameters(index = "4")
	private Integer max_n;

	@Parameters(index = "5")
	private Integer width;

	@Parameters(index = "6")
	private Integer height;

	@Parameters(index = "7")
	private Integer divisions;

	@Parameters(index = "8", arity = "1..*")
	private List<URL> hosts;

	@Override
	public void run() {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		List<SubDivider.SubDivision> subDividedImages = new SubDivider(divisions, divisions).divide(image);

		ExecutorService threadPool = Executors.newCachedThreadPool();
		double realRange = max_c_re - min_c_re;
		double imaginaryRange = max_c_im - min_c_im;

		for (int i = 0; i < subDividedImages.size(); i++) {
			SubDivider.SubDivision subDivision = subDividedImages.get(i);
			BufferedImage subDividedImage = subDivision.getImage();
			URL host = hosts.get(i % hosts.size());

			double xPercent = subDivision.getOriginalMinX() / (double) width;
			double yPercent = subDivision.getOriginalMinY() / (double) height;
			double widthPercent = subDividedImage.getWidth() / (double) width;
			double heightPercent = subDividedImage.getHeight() / (double) height;

			String resource = new StringJoiner("/").add("/mandelbrot")
					.add(Double.toString(min_c_re + realRange * xPercent))
					.add(Double.toString(min_c_im + imaginaryRange * yPercent))
					.add(Double.toString(min_c_re + realRange * xPercent + realRange * widthPercent))
					.add(Double.toString(min_c_im + imaginaryRange * yPercent + imaginaryRange * heightPercent))
					.add(Integer.toString(subDividedImage.getWidth()))
					.add(Integer.toString(subDividedImage.getHeight())).add(Integer.toString(max_n)).toString();

			URL url;
			try {
				url = new URL(host, resource);
			} catch (MalformedURLException exception) {
				System.err.println("MalformedURLException: " + exception.getMessage());
				return;
			}

			threadPool.submit(new SubDivisionGenerator(url, subDividedImage));
		}

		try {
			threadPool.shutdown();
			threadPool.awaitTermination(30, TimeUnit.MINUTES);
		} catch (InterruptedException exception) {
			System.err.println("InterruptedException: " + exception.getMessage());
			return;
		}

		File file = new File("output.pgm");
		HashMap<String, Object> params = new HashMap<>();

		try {
			Imaging.writeImage(image, file, ImageFormats.PGM, params);
			System.out.println("File writing has been completed!");
		} catch (ImageWriteException | IOException exception) {
			System.err.println("IO exception was thrown: " + exception.getMessage());
			return;
		}
	}

}
