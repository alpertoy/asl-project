package io.gitlab.alpertoy.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.alpertoy.service.MandelbrotImpl;

@RestController
public class MandelbrotController {
	
	@Autowired
	MandelbrotImpl mandelbrotImpl;
	
	@GetMapping("/mandelbrot/{min_c_re}/{min_c_im}/{max_c_re}/{max_c_im}/{x}/{y}/{inf_n}")
    public void generateMandelbrot(
            HttpServletResponse response,
            @PathVariable("min_c_re") float min_c_re,
            @PathVariable("max_c_re") float max_c_re,
            @PathVariable("min_c_im") float min_c_im,
            @PathVariable("max_c_im") float max_c_im,
            @PathVariable("x") int width,
            @PathVariable("y") int height,
            @PathVariable("inf_n") int iterations)
            throws IOException {
        response.setContentType("image/jpg");
        OutputStream output = response.getOutputStream();
        BufferedImage bi = mandelbrotImpl.generateFromTerminal(min_c_re, max_c_re, min_c_im, max_c_im, width, height, iterations);
        ImageIO.write(bi, "jpeg", output);
    }

}
