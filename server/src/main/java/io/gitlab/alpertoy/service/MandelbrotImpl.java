package io.gitlab.alpertoy.service;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import org.springframework.stereotype.Service;

@Service
public class MandelbrotImpl extends JComponent {
	
	private int width;
    private int height;
    private int iterations;
    public float originalX = 0;
    public float originalY = 0;

    private BufferedImage bufferedImage;


    public MandelbrotImpl() {

    }

    // create image from terminal
    public BufferedImage generateFromTerminal(float min_c_re, float max_c_re, float min_c_im, float max_c_im, int x, int y, int iterations) {
        bufferedImage = new BufferedImage(x, y, BufferedImage.TYPE_BYTE_GRAY);

        this.width = x;
        this.height = y;
        this.iterations = iterations;

        calculateOriginalX(min_c_re, max_c_re);
        calculateOriginalY(min_c_im, max_c_im);
        return getBufferedImage();

    }

    // calculate buffered image
    private BufferedImage getBufferedImage() {
        for(int x = 0; x < width;x++)
            for(int y = 0;y < height;y++){
                int color = calculatePoint((x - width/2)/ originalX, (y - height/2)/ originalY);

                bufferedImage.setRGB(x, y, color);
            }
        return bufferedImage;
    }

    // calculate original x-axis
    public int calculateOriginalX(float min_c_re, float max_c_re){

    	originalX = width/(max_c_re - min_c_re);
        return (int) originalX;
    }

    // calculate original y-axis
    public int calculateOriginalY(float min_c_im, float max_c_im){

    	originalY = height/(max_c_im - min_c_im);
        return (int) originalY;
    }

    // calculate x,y point re=x, im=y
    public int calculatePoint(float re , float im) {

        float cRe = re;
        float cIm = im;

        int i = 0;

        for(;i< iterations; i++){

            float nRe = re * re - im * im + cRe;
            float nIm = 2 * re * im + cIm;
            re = nRe;
            im = nIm;

            if(re * re + im * im > 4) break;
        }

        if(i == iterations)return 0x0000000;
        return Color.HSBtoRGB(((float)i / iterations ), 0.5f, 1);
    }

}
