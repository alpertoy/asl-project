## How to run

### Server project
build the project using 
`mvn clean install`

Then run the jar file using
`java -jar target/server-0.0.1-SNAPSHOT.jar --server.port=3333`

Run another server using a different port number
`java -jar target/server-0.0.1-SNAPSHOT.jar --server.port=6666`

### Client project
Build CLI project using following command.
`mvn clean compile assembly:single`
Then run following command to run the CLI.
`java -jar target/client-0.0.1-SNAPSHOT-jar-with-dependencies.jar -1 -1.5 2 1.5 100 10000 10000 1 http://localhost:3333`

For simplicity pre-built jars have been included.